<?php
    require 'func.php';
    if (isset($_POST['submit'])) {
        if (tambah($_POST) > 0)
        {
            echo "
                    <script>
                        alert('berhasil menambahkan KTM!');
                        document.location.href = 'menu.php';
                    </script>
                ";
        }else {
            echo mysqli_error(($conn));
            echo "
                    <script>
                        alert('GAGAL!');
                    </script>
                ";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SID Registration</title>
    
    <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs@latest"> </script>
    <!-- Include Required Prerequisites -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
    
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    <!-- from canvas -->
    <!-- =============== Bootstrap Core CSS =============== -->
    <link rel="stylesheet" href="static/css/bootstrap.min.css" type="text/css">
    <!-- =============== Google fonts =============== -->
    <link href='https://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <!-- =============== fonts awesome =============== -->
    <link rel="stylesheet" href="static/css/font-awesome.min.css" type="text/css">
    <!-- =============== Plugin CSS =============== -->
    <link rel="stylesheet" href="static/css/animate.min.css" type="text/css">
    <!-- =============== Custom CSS =============== -->
    <link rel="stylesheet" href="static/css/style.css" type="text/css">
    <!-- =============== Owl Carousel Assets =============== -->
    <link href="static/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="static/owl-carousel/owl.theme.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>
    <!-- =============== Preloader =============== -->
    <!-- <div id="preloader">
        <div id="loading">
        </div>
    </div> -->
    
    <div class="container">
        <span class="angle2"></span>
        <span class="angle"></span>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 wow zoomIn animated headding" data-wow-delay=".1s">
                <h2>Regist<span>ration</span></h2>
                <p>Don't have account yet? Please join us by filling in the personal data below</p>
            </div>
        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-4 wow bounceIn animated" data-wow-delay=".1s">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="ajax-hidden">

                        <div data-wow-delay=".8s" class="form-group wow fadeInUp animated">
                            <label for="nim" class="sr-only">SID</label>
                            <input type="text" placeholder="SID" name="nim" class="form-control" id="nim" required>
                        </div>

                        <div data-wow-delay=".9s" class="form-group wow fadeInUp animated">
                            <label for="nama" class="sr-only">Full Name</label>
                            <input type="text" placeholder="Full Name" name="nama" class="form-control" id="nama" required>
                        </div>

                        <div data-wow-delay="1s" class="form-group wow fadeInUp animated">
                            <label for="fakultas" class="sr-only">Faculty</label>
                            <input type="text" placeholder="Faculty" name="fakultas" class="form-control" id="fakultas" required>
                        </div>

                        <div data-wow-delay="1.1s" class="form-group wow fadeInUp animated">
                            <label for="progdi" class="sr-only">Study Program</label>
                            <input type="text" placeholder="Study Program" name="progdi" class="form-control" id="progdi" required>
                        </div>

                        <div data-wow-delay="1.2s" class="form-group wow fadeInUp animated">
                            <label for="angkatan" class="sr-only">Class of</label>
                            <input type="text" placeholder="Class of" name="angkatan" class="form-control" id="angkatan" required>
                        </div>

                        <div data-wow-delay="1.3s" class="form-group wow fadeInUp animated">
                            <label for="kota_asal" class="sr-only">Hometown</label>
                            <input type="text" placeholder="Hometown" name="kota_asal" class="form-control" id="kota_asal" required>
                        </div>

                        <div data-wow-delay="1.4s" class="form-group wow fadeInUp animated">
                            <label for="kota_lahir" class="sr-only">Place of Birth</label>
                            <input type="text" placeholder="Place of Birth" name="kota_lahir" class="form-control" id="kota_lahir" required>
                        </div>
                        <div data-wow-delay="1.5s" class="form-group wow fadeInUp animated">
                            <label for="lahir" class="sr-only">Date of Birth</label>
                            <input type="date" name="lahir" id="lahir" value="" class="form-control" min="1900-01-01" required>
                        </div>


                        <div data-wow-delay="1.6s" class="form-group wow fadeInUp animated">
                            <label for="sex">Gender : </label>
                            <input type="radio" name="sex" value="1"><label>&nbsp;Man&nbsp;&nbsp;&nbsp;</label>
                            <input type="radio" name="sex" value="0"><label>&nbsp;Woman</span></label>
                        </div>
                        <div data-wow-delay="1.7s" class="form-group wow fadeInUp animated">
                            <label for="gambar" class="sr-only">Photo</label>
                            <input type="file" name="gambar" id="gambar" class="form-control" required>
                        </div>

                        <button data-wow-delay="1.8s" class="btn btn-sm btn-block wow fadeInUp animated" type="submit" name="submit">SUBMIT</button>
                    </div>
                    <div class="ajax-response"></div>
                </form>

            </div>

            <div class="col-xs-12 col-sm-3 col-md-4 wow bounceIn animated map" data-wow-delay=".5s">

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1978.6572067904165!2d110.49932091619031!3d-7.31853463828657!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a79c10062ef89%3A0x935ee5fe27504b6d!2sGedung%20C%20UKSW!5e0!3m2!1sid!2sid!4v1596116913618!5m2!1sid!2sid" width="100%" height="100%"
                    frameborder="0" style="border:0" allowfullscreen></iframe>

                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d6508922.473104964!2d-123.76275651635396!3d37.19583981824279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sCalifornia%2C+United+States!5e0!3m2!1sen!2sin!4v1450994260631" width="100%" height="250"
                    frameborder="0" style="border:0" allowfullscreen></iframe> -->

                <!-- <script src="https://platform.linkedin.com/badges/js/profile.js" async defer type="text/javascript"></script> -->
            </div>

            <div class="col-xs-12 col-sm-3 col-md-4 wow bounceIn animated" data-wow-delay=".6s">
                <div class="badge-base LI-profile-badge" data-locale="en_US" data-size="medium" data-theme="dark" data-type="VERTICAL" data-vanity="jane-chrestella-marutotamtama" data-version="v1"><a class="badge-base__link LI-simple-link" href="https://id.linkedin.com/in/jane-chrestella-marutotamtama?trk=profile-badge"> </a></div>
            </div>
        </div>
    </div>
    <script src="https://platform.linkedin.com/badges/js/profile.js" async defer type="text/javascript"></script>
        
</body>

</html>

<!-- <h1>Welcome!</h1>
    <h2>Please register your SID here</h2>
    <form action="" method="post" enctype="multipart/form-data">
        <ul>
            <li>
                <label for="nim">SID : </label>
                <input type="text" name="nim" id="nim" required>
            </li>
            <li>
                <label for="nama">Full Name: </label>
                <input type="text" name="nama" id="nama" required>
            </li>
            <li>
                <label for="fakultas">Faculty : </label>
                <input type="text" name="fakultas" id="fakultas" required>
            </li>
            <li>
                <label for="progdi">Study Program : </label>
                <input type="text" name="progdi" id="progdi" required>
            </li>
            <li>
                <label for="angkatan">Class of : </label>
                <input type="text" name="angkatan" id="angkatan" required>
            </li>
            <li>
                <label for="kota_asal">Hometown : </label>
                <input type="text" name="kota_asal" id="kota_asal" required>
            </li>
            <li>
                <label for="kota_lahir">Place of Birth : </label>
                <input type="text" name="kota_lahir" id="kota_lahir" required>
            </li>
            <li>
                <label for="lahir">Date of Birth : </label>
                <input type="text" name="lahir" id="lahir" required>
                <input type="date" name="lahir" id="lahir" value="" min="1900-01-01">
            </li>
            <li>
                <label for="sex">Gender : </label>
                <input type="text" name="sex" id="sex" required>
                <input type="radio" name="sex" value="1"><label >Man</label>
                <input type="radio" name="sex" value="0"><label >Woman</label>
            </li>
            <li>
                <label for="gambar">Photo : </label>
                <input type="file" name="gambar" id="gambar" required>
            </li>
            <li>
                <button type="submit" name="submit" >SUBMIT!</button>
            </li>
        </ul>
    </form> -->