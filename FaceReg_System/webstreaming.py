# python3 webstreaming.py --ip 0.0.0.0 --port 8000
# import the necessary packages
from flask.wrappers import Request
from singlemotiondetector import SingleMotionDetector
from flask import Response
from flask import Flask, request
from flask import render_template
# from flask import redirect, url_for, request
from imutils.video import VideoStream
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from threading import Thread

from face_recognition.face_recognition_cli import image_files_in_folder

import threading
import argparse
import datetime
import imutils
import time
import cv2
import os as os
import os as os1
import os.path
import face_recognition
import numpy as np
import pickle


# initialize the output frame and a lock used to ensure thread-safe
# exchanges of the output frames (useful when multiple browsers/tabs
# are viewing the stream)
outputFrame = None
lock = threading.Lock()
b = True
nim = ''
# initialize a flask object
app = Flask(__name__)

# initialize the video stream and allow the camera sensor to
# warmup
# vs = VideoStream(usePiCameknown_face_imgsra=1).start()
print("[INFO] starting video stream...")

#LOADDD=================================
ENCODING_PATH = "/opt/lampp/htdocs/skripsi_coba1/encodings/"
IMG_PATH = "/opt/lampp/htdocs/skripsi_coba1/imgDB/"  # "http://192.168.2.203:2001/imgDB/"
os1.chdir(ENCODING_PATH)

def update_encodings():
    list_img_folder = [f.path for f in os.scandir(IMG_PATH) if f.is_dir() ]

    for i in range(len(os.listdir(IMG_PATH))) :
        if os1.path.isdir(list_img_folder[i][-9:]) == False:
            update_dir = list_img_folder[i]
            name = list_img_folder[i][-9:]
            for img_path in image_files_in_folder(update_dir):
                frame = cv2.imread(img_path)

                if frame.shape[0] > 1000 and frame.shape[1] > 1000:
                    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
                else:
                    small_frame = frame
                rgb_small_frame = small_frame[:, :, ::-1]
                face_bounding_boxes = face_recognition.face_locations(rgb_small_frame)
                print(len(face_bounding_boxes))
                if len(face_bounding_boxes)!=1:
                    print("Photo "+ name +" not suitable")
                else:
                    encoding = face_recognition.face_encodings(small_frame, known_face_locations=face_bounding_boxes)[0]
                    print(len(encoding))

                    location = ENCODING_PATH + name
                    print(location)
                    if not os.path.isdir(location):
                        os.makedirs(location)
                        simpan = open(str(location)+'/'+name+ '.txt','w')
                        np.savetxt(simpan,encoding)
                        simpan.close()
                        print("Encoding " + name +" Saved")
        

def load():
    if len(os.listdir(ENCODING_PATH)) != len(os.listdir(IMG_PATH)) :
        print("UPDATE ENCODING")    
        update_encodings()
        print("UDAH UPDATE ENCODING")    
        

    print("load 2")
    known_face_encodings = []
    known_face_names = []

    for class_dir in os.listdir(ENCODING_PATH):
        #encodings
        info = ENCODING_PATH + class_dir + "/" + class_dir + ".txt"
        original_encoding = np.loadtxt(info)
        known_face_encodings.append(original_encoding)
        known_face_names.append(class_dir)
    return known_face_names, known_face_encodings

#======================================

known_face_names = []
known_face_encodings = []
known_face_imgs_enc = []

face_locations = []
face_encodings = []
face_names = []
    
    
if len(known_face_encodings) == 0:
        known_face_names, known_face_encodings = load()

vs = VideoStream(src=0).start()  
# vs = cv2.VideoCapture('/home/janeangel90/Documents/SKRIPSI/video/hp_jane.mp4') 
time.sleep(2.0)


@app.route("/", methods=["POST", "GET"])
def index():
    # return the rendered template
    global nim, TEXTT, fix_facedist
    nim = request.args.get('nim')
    # start a thread that will perform motion detection
    # t = threading.Thread(target=facereg_liveness, args=(
    # 	args["frame_count"],)) #detect_motion #custom_database #facereg_liveness
    # t.daemon = True
    # t.start()
    # return render_template("index.html", sim=str(face_distances))

    # with class
    twrv = ThreadWithReturnValue(
        target=facereg_liveness, args=(args["frame_count"],))
    twrv.daemon = True
    twrv.start()

    time.sleep(5.0)
    # wait_until(TEXTT == "NOT VERIFF" or TEXTT == "VERIFF", 10)
    # waitUntil(TEXTT == "NOT VERIFF" or TEXTT == "VERIFF")
    # if TEXTT == "NOT VERIFF" or TEXTT == "VERIFF" :
    #     print("---------------------------")
    #     print(TEXTT)
    if TEXTT == "VERIFIED":
        return render_template("index.html", sim=TEXTT, sim2=fix_facedist)
    if TEXTT == "NOT VERIFIED":
        return render_template("index_false.html", sim=TEXTT, sim2=fix_facedist)
    # else :
    #     return render_template("index.html")

def wait_until(somepredicate, timeout, period=0.25):
  mustend = time.time() + timeout
  while time.time() < mustend:
    if somepredicate: return True
    time.sleep(period)
  return False

def waitUntil(condition): #defines function
    wU = True
    while wU == True:
        if condition:
            wU = False
            if TEXTT == "VERIFIED":
                return render_template("index.html", sim=TEXTT, sim2=fix_facedist)
            if TEXTT == "NOT VERIFIED":
                return render_template("index_false.html", sim=TEXTT, sim2=fix_facedist)
        time.sleep(1) #waits 60s for preformance

def detect_motion(frameCount):
    # grab global references to the video stream, output frame, and
    # lock variables
    global vs, outputFrame, lock
    # initialize the motion detector and the total number of frames
    # read thus far
    md = SingleMotionDetector(accumWeight=0.1)
    total = 0

    # loop over frames from the video stream
    while True:
        # read the next frame from the video stream, resize it,
        # convert the frame to grayscale, and blur it
        frame = vs.read()
        frame = imutils.resize(frame, width=400)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (7, 7), 0)

    # grab the current timestamp and draw it on the frame
        timestamp = datetime.datetime.now()
        cv2.putText(frame, timestamp.strftime(
            "%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10),
            cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

        # if the total number of frames has reached a sufficient
        # number to construct a reasonable background model, then
        # continue to process the frame
        if total > frameCount:
            # detect motion in the image
            motion = md.detect(gray)

# check to see if motion was found in the frame
            if motion is not None:
                # unpack the tuple and draw the box surrounding the
                # "motion area" on the output frame
                (thresh, (minX, minY, maxX, maxY)) = motion
                cv2.rectangle(frame, (minX, minY), (maxX, maxY),
                              (0, 0, 255), 2)

        # update the background model and increment the total number
        # of frames read thus far
        md.update(gray)
        total += 1

    # acquire the lock, set the output frame, and release the
        # lock
        with lock:
            outputFrame = frame.copy()


def custom_database(frameCount):
    front_face_cascpath = (cv2.data.haarcascades +
                           'haarcascade_frontalface_default.xml')
    detector = cv2.CascadeClassifier(front_face_cascpath)

    # grab global references to the video stream, output frame, and
    # lock variables
    global vs, outputFrame, lock

# initialize the motion detector and the total number of frames
    # read thus far
    md = SingleMotionDetector(accumWeight=0.1)
    total = 0
    
    # loop over frames from the video stream
    while True:
        frame = vs.read()
        orig = frame.copy
        frame = imutils.resize(frame, width=400)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        if total > frameCount:
            # detect faces in the grayscale frame
            rects = detector.detectMultiScale(gray, scaleFactor=1.1,
                                              minNeighbors=5, minSize=(30, 30))

            # loop over the face detections and draw them on the frame
            for (x, y, w, h) in rects:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # update the background model and increment the total number
        # of frames read thus far
        md.update(gray)
        total += 1

    # acquire the lock, set the output frame, and release the
        # lock
        with lock:
            outputFrame = frame.copy()

#########################################################


# def load(class_dir):
#     print("load")
#     known_face_names = []
#     known_face_img_encs = []
#     known_face_names.append(class_dir)
#     info2 = IMG_PATH + class_dir + "/" + class_dir + ".jpg"
#     frame = cv2.imread(info2)
#     if frame.shape[0] > 1000 and frame.shape[1] > 1000:
#         small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
#     else:
#         small_frame = frame
#     rgb_small_frame = small_frame[:, :, ::-1]
#     face_bounding_boxes = face_recognition.face_locations(rgb_small_frame)
#     if len(face_bounding_boxes) == 1:
#         encoding = face_recognition.face_encodings(small_frame, known_face_locations=face_bounding_boxes)[0]
#         known_face_img_encs.append(encoding)
#     return known_face_names, known_face_img_encs


def facerec(frame, counter, known_face_encodings, process_this_frame):
    global name, face_locations, face_names, face_distances
    name = ''
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
    rgb_small_frame = small_frame[:, :, ::-1]
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(
            rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(
                known_face_encodings, face_encoding)
            name = "Unknown"

            # Or instead, use the known face with the smallest distance to the new face
            face_distances = face_recognition.face_distance(
                known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]
            face_names.append(name)
    counter = counter+1
    if counter == 30:
        counter = 0
        process_this_frame = not process_this_frame

def facerec2(frame, counter, known_face_encodings, process_this_frame):
    global name, face_locations, face_names, face_distances, face_encodings, score
    # name = ''
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
    rgb_small_frame = small_frame[:, :, ::-1]
        
    if process_this_frame:
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)
        face_names = []
        for face_encoding in face_encodings:
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
            name = "Unknown"
            # score = "Unknown"
            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                score = str(round(face_distances[best_match_index],3))
                # print(score)
                name = known_face_names[best_match_index]
                if float(score) > 0.49 :
                    name = "Unknown"
                    score = 9999
            face_names.append(name)

    counter = counter+1
    if counter == 30:
        counter = 0
        process_this_frame = not process_this_frame

def liveness(frame2):
    (w, h, c) = frame2.shape
    frame2 = cv2.resize(frame2, (600, h))
    (h, w) = frame2.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame2, (300, 300)), 1.0,
                                 (300, 300), (104.0, 177.0, 123.0))
    net.setInput(blob)
    detections = net.forward()

    for i in range(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > 0.5:
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            startX = max(0, startX)
            startY = max(0, startY)
            endX = min(w, endX)
            endY = min(h, endY)

            face = frame2[startY:endY, startX:endX]
            face = cv2.resize(face, (32, 32))
            face = face.astype("float") / 255.0
            face = img_to_array(face)
            face = np.expand_dims(face, axis=0)

            preds = model.predict(face)[0]
            j = np.argmax(preds)
            global label, LABEL
            label = le.classes_[j]
            LABEL = label
            label = "{}: {:.4f}".format(label, preds[j])


def facereg_liveness(frameCount):
    # grab global references to the video stream, output frame, and
    # lock variables
    global vs, outputFrame, lock

    # facereg_liveness
    global known_face_names, counter, known_face_encodings, score
    global net, model, le, nim, RET, TEXTT, fix_facedist
    TEXTT = "a"
    fix_facedist = 0
    # known_face_names = []
    # known_face_encodings = []
    # known_face_imgs_enc = []
    process_this_frame = True
    counter = 0
    RET = ''

    protoPath = os.path.sep.join(["/opt/lampp/htdocs/skripsi_coba1/face_detector", "deploy.prototxt"])
    modelPath = os.path.sep.join(
        ["/opt/lampp/htdocs/skripsi_coba1/face_detector", "res10_300x300_ssd_iter_140000.caffemodel"])
    net = cv2.dnn.readNetFromCaffe(protoPath, modelPath)
    model = load_model("/opt/lampp/htdocs/skripsi_coba1/liveness.model")
    # le = pickle.loads(open("le.pickle", "rb").read()) #first spoofing
    # le = pickle.loads(open("lee.pickle", "rb").read()) #second spoofing
    le = pickle.loads(open("/opt/lampp/htdocs/skripsi_coba1/le_skripsi.pickle", "rb").read()) #third spoofing
    global video_capture, frame

    # initialize the motion detector and the total number of frames
    # read thus far
    md = SingleMotionDetector(accumWeight=0.1)
    total = 0
    TOT = 0
    TOT_unk = 0
    sum_facedist = 0
    mus_facedist = 0

    # if len(known_face_imgs_enc) == 0:
    #     known_face_names, known_face_imgs_enc = load(nim)
    START1 = time.time()
    while True:
        # ret, 
        frame = vs.read() #INI YANG DITAMBAHIN rEt
        # frame = imutils.resize(frame, width=600)
        # if ret == True :
        if total > frameCount:
            # pake ini buat .txt
            # threading.Thread(target=facerec(frame, 0, known_face_encodings,process_this_frame)).start()
            # pake ini buat .jpg
            START = time.time()
            # threading.Thread(target=facerec(frame, 0, known_face_imgs_enc, process_this_frame)).start()
            threading.Thread(target=facerec2(frame, 0, known_face_encodings, process_this_frame)).start()
            threading.Thread(target=liveness(frame)).start()

            # loop over the face detections and draw them on the frame
            for (top, right, bottom, left), name in zip(face_locations, face_names):
                top *= 4
                right *= 4
                bottom *= 4
                left *= 4
                font = cv2.FONT_HERSHEY_DUPLEX
                if LABEL == "real" :
                    # Draw a box around the face
                    cv2.rectangle(frame, (left, top),
                                (right, bottom), (0, 255, 0), 2)
                    # Draw a label with a name below the face
                    cv2.rectangle(frame, (left, bottom - 35),
                                (right, bottom), (0, 255, 0), cv2.FILLED)

                    if name == nim :
                        sum_facedist += float(score)
                        TOT += 1
                    else :
                        mus_facedist += float(score)
                        TOT_unk += 1
                elif LABEL == "fake" :
                    TOT_unk += 1

                    if name == nim :
                        sum_facedist += float(score)
                    else :
                        mus_facedist += float(score)
                    # Draw a box around the face
                    cv2.rectangle(frame, (left, top),
                                (right, bottom), (0, 0, 255), 2)
                    # Draw a label with a name below the face
                    cv2.rectangle(frame, (left, bottom - 35),
                                (right, bottom), (0, 0, 255), cv2.FILLED)
                
                cv2.putText(frame, name, (left + 6, bottom - 6),
                            font, 0.8, (0, 0, 0), 1)
                cv2.putText(frame, str(score), (right + 6, top - 6), 
                            font, 1.0, (255, 255, 255), 1)
                # cv2.putText(frame, score, (left + 6, top - 6), font, 0.8, (255, 255, 255), 1)
                cv2.putText(frame, label, (left, top-10),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            
            STOP = time.time()
            elap = (STOP - START)
            print("[INFO] single frame took {:.4f} seconds".format(elap))
            print("TOT : " + str(TOT))
            # print("SUM FACE : " + str(sum_facedist))
            print("TOT UNK: " + str(TOT_unk))

            if TOT >= 6 :
                fix_facedist = sum_facedist/TOT
                fix_facedist2 = mus_facedist/TOT
                STOP2 = time.time()
                tot_elap = (STOP2 - START1)
                print("FIXXX : " + str(fix_facedist))
                print("FIX_SALAH : " + str(fix_facedist2))
                print("[INFO] ALL {:.4f} seconds".format(tot_elap))
                # print("[INFO] RATA2 {:.4f} seconds".format(tot_elap/TOT))
                print("TERVERIFIKASI")
                TEXTT = "VERIFIED"
                TOT = 0
                sum_facedist = 0
                TOT_unk = 0
                mus_facedist = 0
                return f"<h1>{TEXTT}</h1><h2>LOL</h2>"
            if TOT_unk >= 6 :
                fix_facedist2 = mus_facedist/TOT_unk
                fix_facedist = sum_facedist/TOT_unk
                STOP3 = time.time()
                tot_elap = (STOP3 - START1)
                print("FIXXX : " + str(fix_facedist))
                print("FIX_SALAH : " + str(fix_facedist2))
                print("[INFO] ALL {:.4f} seconds".format(tot_elap))
                # print("[INFO] RATA2 {:.4f} seconds".format(tot_elap/TOT_unk))
                print("TIDAK TERVERIFIKASI")
                TEXTT = "NOT VERIFIED"
                TOT = 0
                sum_facedist = 0
                TOT_unk = 0
                mus_facedist = 0
                return f"<h1>{TEXTT}</h1><h2>LOL</h2>"
        # update the background model and increment the total number
        # of frames read thus far
        md.update(frame)
        total += 1

    # acquire the lock, set the output frame, and release the
        # lock
        with lock:
            outputFrame = frame.copy()
			
# class


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        print(type(self._target))
        if self._target is not None:
            self._return = self._target(*self._args,
                                        **self._kwargs)

    def join(self, *args):
        Thread.join(self, *args)
        return self._return


def generate():
    # grab global references to the output frame and lock variables
    global outputFrame, lock

    # loop over frames from the output stream
    while True:
        # wait until the lock is acquired
        with lock:
            # check if the output frame is available, otherwise skip
            # the iteration of the loop
            if outputFrame is None:
                continue

            # encode the frame in JPEG format
            (flag, encodedImage) = cv2.imencode(".jpg", outputFrame)

            # ensure the frame was successfully encoded
            if not flag:
                continue

    # yield the output frame in the byte format
        yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
              bytearray(encodedImage) + b'\r\n')


@app.route("/video_feed")
def video_feed():
    # return the response generated along with the specific media
    # type (mime type)
    return Response(generate(),
                    mimetype="multipart/x-mixed-replace; boundary=frame")


# check to see if this is the main thread of execution
if __name__ == '__main__':
    # construct the argument parser and parse command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--ip", type=str, default="0.0.0.0",
                    help="ip address of the device")
    ap.add_argument("-o", "--port", type=int, default="8000",
                    help="ephemeral port number of the server (1024 to 65535)")
    ap.add_argument("-f", "--frame-count", type=int, default=32,
                    help="# of frames used to construct the background model")
    args = vars(ap.parse_args())

    # start the flask app
    app.run(host=args["ip"], port=args["port"], debug=True,
            threaded=True, use_reloader=False)


# release the video stream pointer
vs.stop()
