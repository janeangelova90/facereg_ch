<?php
    error_reporting( E_ALL );
    ini_set( "display_errors", 1 );
    $conn = mysqli_connect("localhost","root","","mahasiswa");
    // $conn = mysqli_connect("iki-face-reg-int.mariadb.ap-southeast-5.rds.aliyuncs.com:3306","adm_facereg","4dM_7aC3Reg123","ktp");

    function query($query) {
        global $conn;
        $result = mysqli_query($conn, $query);
        $rows = [];
        while( $row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }

    function tambah($data) {
        global $conn;
        $nim = htmlspecialchars($data["nim"]);
        $nama = htmlspecialchars($data["nama"]);
        $fakultas = htmlspecialchars($data["fakultas"]);
        $progdi = htmlspecialchars($data["progdi"]);
        $angkatan = htmlspecialchars($data["angkatan"]);
        $kota_asal = htmlspecialchars($data["kota_asal"]);
        $kota_lahir = htmlspecialchars($data["kota_lahir"]);
        $lahir = htmlspecialchars($data["lahir"]);

        $riwayat = upload_txt($nim);
        if(!$riwayat) {
            return false;
        }
        $gambar = upload($nim);
        if(!$gambar) {
            return false;
        }

        $sex = htmlspecialchars($data["sex"]);
        if ($sex == "1") {
            $query = "INSERT INTO mahasiswa VALUES
                (NULL,'$nama','$nim','$fakultas','$progdi','$angkatan',
                true,'$kota_asal','$lahir','$kota_lahir','123')
                ";
        }else if ($sex == "0") {
            $query = "INSERT INTO mahasiswa VALUES
                (NULL,'$nama','$nim','$fakultas','$progdi','$angkatan',
                false,'$kota_asal','$lahir','$kota_lahir','123')
                ";
        }

        mysqli_query($conn,$query);
        return mysqli_affected_rows($conn);
    }

    function upload_txt($nim) {
        mkdir ("./riwayat/".$nim.'/', "0777");
        // mkdir ("./riwayat/".$nim.'/', "0777",true);
        $output = shell_exec('chmod -R 777 /opt/lampp/htdocs/skripsi_coba1/riwayat/'.$nim.'/');
        echo "<pre>$output</pre>";
        $namaFileBaru = $nim.'.txt';
        $myfile = fopen('./riwayat/'.$nim.'/'.$namaFileBaru, "w") or die("Unable to open file!");
        $txt = "User dibuat pada ";
        fwrite($myfile, $txt);
        fwrite($myfile,date('Y-m-d H:i:s') . ': ' . $_SERVER['REMOTE_ADDR']."\n"); 
        fclose($myfile);
        return $namaFileBaru;
    }
    
    function upload($nim) {
        $namaFile = $_FILES['gambar']['name'];
        $ukuranFile = $_FILES['gambar']['size'];
        $error = $_FILES['gambar']['error'];
        $tmpName = $_FILES['gambar']['tmp_name'];

        if($error === 4) {
            echo "<script>
                    alert('pilih gambar terlebih dahulu!');
                  </script>";
            return false;
        }

        $validimg = ['jpg','jpeg','png'];
        $ekstensi = explode('.', $namaFile);
        $ekstensi = strtolower(end($ekstensi));
        $format = pathinfo($namaFile, PATHINFO_EXTENSION); //$format menyimpan ekstensi file.
        if( !in_array ($format,$validimg)) { //($ekstensi,$validimg)) {
            echo "<script>
                    alert('upload gambar jangan yg lain!');
                  </script>";
            return false;
        }
        if ( $ukuranFile > 2000000) {
            echo "<script>
                    alert('ukuran gambar terlalu besar! maks 2MB');
                  </script>";
            return false;
        }
        
        //folder
        mkdir ("./img/".$nim.'/', "0777");
        $output = shell_exec('chmod -R 777 /opt/lampp/htdocs/skripsi_coba1/img/'.$nim.'/');
        // $output = shell_exec('chmod -R 777 /var/www/html/ktp-facereg/img/'.$nim.'/');
        echo "<pre>$output</pre>";

        // $dafnim = query("SELECT nik FROM warga");
        $namaFileBaru = $nim.'.jpg';#.$format;
        move_uploaded_file($tmpName, 'img/' .$nim.'/'. $namaFileBaru);
        return $namaFileBaru;
    }

    function hapus($id) {
        global $conn;
        mysqli_query($conn, "DELETE FROM mahasiswa WHERE id = $id");
        return mysqli_affected_rows($conn);
    }
    
    function ubah($data) {
        global $conn;
        $id = $data["id"];
        $nim = htmlspecialchars($data["nim"]);
        $nama = htmlspecialchars($data["nama"]);
        $email = htmlspecialchars($data["email"]);
        $jurusan = htmlspecialchars($data["jurusan"]);
        $imglama = htmlspecialchars($data["imglama"]);
        $gambar = htmlspecialchars($data["gambar"]);

        if ($_FILES['gambar']['error'] === 4) {
            $gambar = $imglama;
        } else {
            $gambar = upload($nim);
        }

        $query = "UPDATE mahasiswa SET
                    nim ='$nim',
                    nama = '$nama',
                    email = '$email',
                    jurusan = '$jurusan',
                    gambar = '$gambar'
                  WHERE id = $id
                ";

        mysqli_query($conn,$query);
        return mysqli_affected_rows($conn);
    }

    function cari($keyword) {
        $query = "SELECT * FROM warga
                    WHERE 
                    nama LIKE '%$keyword%' OR
                    nik LIKE '%$keyword%' OR
                    prov LIKE '%$keyword%' OR
                    kota LIKE '%$keyword%' OR
                    kel LIKE '%$keyword%' OR
                    kec LIKE '%$keyword%' OR
                    addr LIKE '%$keyword%' 
                 ";
        return query($query);
    }

    // function registrasi($data) {
    //     global $conn;
    //     $email = htmlspecialchars($data["email"]);
    //     $nama = htmlspecialchars($data["nama"]);
    //     $gambar = htmlspecialchars($data["email"]).'.jpg';
    //     $password = mysqli_real_escape_string($conn, $data["password"]);
    //     $password2 = mysqli_real_escape_string($conn, $data["password2"]);
    //     $saldo = 0;
    //     $riwayat = '';

    //     // $gambar = upload($email);
    //     // if(!$gambar) {
    //     //     return false;
    //     // }

    //     $result = mysqli_query($conn, "SELECT email FROM user 
    //                 WHERE email = '$email'");
    //     if (mysqli_fetch_assoc($result)) {
    //         echo "  <script>
    //                     alert('email anda sudah terdaftar!');
    //                 </script> ";
    //         return false;
    //     }

    //     if ($password !== $password2) {
    //         echo "  <script>
    //                     alert('konfirmasi password tidak sesuai!');
    //                 </script> ";
    //         return false;
    //     }

    //     $password = password_hash($password, PASSWORD_DEFAULT);
    //     // var_dump($password);
    //     $query = "INSERT INTO user VALUES
    //             (NULL,'$nama','$password',
    //             '$email','$gambar',
    //             '$saldo','$riwayat')";

    //     mysqli_query($conn,$query);
    //     return mysqli_affected_rows($conn);

    // }

?>

