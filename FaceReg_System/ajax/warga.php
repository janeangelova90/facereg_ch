<?php
    require '../func.php';
    $keyword = $_GET["keyword"];
    $query = "SELECT * FROM mahasiswa
                WHERE 
                nama LIKE '%$keyword%' OR
                nim LIKE '%$keyword%' OR
                fakultas LIKE '%$keyword%' OR
                progdi LIKE '%$keyword%' OR
                angkatan LIKE '%$keyword%' OR
                kota_asal LIKE '%$keyword%'
             ";
    $warga = query($query);
?>

<table class="table">
    <thead class="thead-primary">
        <tr>
            <th>No. </th>
            <th>SID</th>
            <!-- <th>Gambar</th> -->
            <th>Name</th>
            <th>Faculty</th>
            <th>Study Program</th>
            <th>Class of</th>
            <th>Gender</th>
            <th>Hometown</th>
            <th>Ttgl</th>
            <th>History</th>
        </tr>
    </thead>
    <tbody>

        <?php $i = 1;?>
        <?php foreach( $warga as $row) : ?>

        <tr class="alert" role="alert">
            <td><?= $i; ?></td>
            <!-- <td>
            <a href="ubah.php?id=<?= $row["id"] ?>">ubah</a>
            <a href="hapus.php?id=<?= $row["id"] ?>">hapus</a>
            </td> -->
            <!-- <td><img src="img/<?= $row["gambar"];?>" width="200px"></td> -->
            <!-- <td><a href="utama.php?nik=<?= $row["nim"] ?>"><?= $row["nim"] ?></a></td> -->
            
            <td><a href="http://localhost:8000?nim=<?= $row["nim"] ?>"><?= $row["nim"] ?></a></td>
            <td><?= $row["nama"] ?></td>
            <td><?= $row["fakultas"] ?></td>
            <td><?= $row["progdi"] ?></td>
            <td><?= $row["angkatan"] ?></td>

            <?php if ($row["sex"] == 1) : ?>
                <td>Man</td>
            <?php else : ?>
                <td>Woman</td>
            <?php endif; ?>

            <td><?= $row["kota_asal"] ?></td>
            <td><?= $row["kota_lahir"].", ".$row["lahir"] ?></td>
            <td><a href="http://localhost/skripsi_coba1/riwayat/<?= $row["nim"] ?>/<?= $row["nim"] ?>.txt"><?= $row["nim"] ?>.txt</a></td>
        
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?>
    </tbody>
</table>

