<?PHP
    require 'func.php';
    $warga  = query("SELECT * FROM mahasiswa");

    if (isset($_POST["cari"])) {
        $warga = cari($_POST["keyword"]);
    }
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>SWCU Attendance System</title>

        <link rel="stylesheet" href="static/css/bootstrap.min.css" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="static/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="static/css/animate.min.css" type="text/css">
        <link rel="stylesheet" href="static/css/style_ori.css" type="text/css">
        <link href="static/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="static/owl-carousel/owl.theme.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="static/css/style_ori.css">
    </head>

    <body>
        <!-- =============== Preloader =============== -->
        <div id="preloader">
            <div id="loading">
            </div>
        </div>
        <!-- =============== nav =============== -->
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="https://www.uksw.edu/"><img src="img/logo.png" alt="Logo"></a>
                        <a class="navbar-brand" href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                        <a class="navbar-brand" href="https://ece.uksw.edu/"><img src="img/ftek.png" alt="Logo"></a>
                        <a class="navbar-brand" href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                        <!-- <div class="badge-base LI-profile-badge" data-locale="en_US" data-size="medium" data-theme="light" data-type="VERTICAL" data-vanity="jane-chrestella-marutotamtama" data-version="v1"><a class="badge-base__link LI-simple-link" href="https://id.linkedin.com/in/jane-chrestella-marutotamtama?trk=profile-badge">Jane Chrestella Marutotamtama</a></div> -->
              
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a class="page-scroll" href="#about">List</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#how-it-works">How it works</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#Screenshots">Screenshots</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#contact">Contact</a>
                            </li>
                            <li>
                            <a class="page-scroll" href="registrasi.php">Registration</a>
                            </li>

                        </ul>
                    </div>
                    <!-- =============== navbar-collapse =============== -->
                </div>
            </div>
            <!-- =============== container-fluid =============== -->
        </nav>


        <!-- =============== header =============== -->
        <header>
            <!-- =============== container =============== -->
            <div class="container">
                <div class="header-content row">
                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <h2 class="wow bounceIn animated" data-wow-delay=".40s">ATTENDANCE SYSTEM</h2>
                        <h3 class="wow bounceIn animated" data-wow-delay=".50s">SATYA WACANA CHRISTIAN UNIVERSITY</h3>
                        <p class="wow bounceIn animated" data-wow-delay=".60s">The following is a display of the SWCU attendance system created by Jane Chrestella Marutotamtama</p>
                        
                        <!-- <p>
                        <div class="btn btn-primary btn-lg btn-ornge wow bounceIn animated" data-wow-delay="1s"><i class="hbtn"></i><a href="https://www.uksw.edu/"><span>SWCU OFFCIAIL</span></a><i class="fa fa-cloud-download"></i>
                        </div>
                    </p> -->
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 wow slideInLeft animated">
                        <img src="img/facereg.png" alt="phones" />
                    </div>
                </div>
            </div>
            <!-- =============== container end =============== -->
        </header>


        <!-- =============== About =============== -->
        <section id="about" class="">
            <!-- =============== container =============== -->
            <div class="container">
                <span class="angle2"></span>
                <span class="angle"></span>
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 wow fadeInLeft animated" data-wow-delay=".5s">
                        <h1><span>HEY THERE!</span> WELCOME TO SWCU ATTENDANCE WEBSITE</h2>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 wow fadeInRight animated" data-wow-delay=".5s">
                        <h2>Please fill in your SID/name:</h2>
                        <br><br>
                        <form action="" method="post">
                            <input type="text" name="keyword" class="form-control" autofocus placeholder="SID/name" autocomplete="off" id="keyword">
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="table-wrap">
                        <div id="container">
                        <table class="table" border="1" cellpadding="10" cellspacing="0">
                            <thead class="thead-primary">
                                <tr>
                                    <th>No. </th>
                                    <th>SID</th>
                                    <!-- <th>Gambar</th> -->
                                    <th>Name</th>
                                    <th>Faculty</th>
                                    <th>Study Program</th>
                                    <th>Class of</th>
                                    <th>Gender</th>
                                    <th>Hometown</th>
                                    <th>Ttgl</th>
                                    <th>History</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php $i = 1;?>
                                <?php foreach( $warga as $row) : ?>

                                <tr class="alert" role="alert">
                                    <td><?= $i; ?></td>
                                    <!-- <td>
                                    <a href="ubah.php?id=<?= $row["id"] ?>">ubah</a>
                                    <a href="hapus.php?id=<?= $row["id"] ?>">hapus</a>
                                    </td> -->
                                    <!-- <td><img src="img/<?= $row["gambar"];?>" width="200px"></td> -->
                                    <!-- <td><a href="utama.php?nik=<?= $row["nim"] ?>"><?= $row["nim"] ?></a></td> -->
                                    
                                    <td><a href="http://localhost:8000?nim=<?= $row["nim"] ?>"><?= $row["nim"] ?></a></td>
                                    <td><?= $row["nama"] ?></td>
                                    <td><?= $row["fakultas"] ?></td>
                                    <td><?= $row["progdi"] ?></td>
                                    <td><?= $row["angkatan"] ?></td>

                                    <?php if ($row["sex"] == 1) : ?>
                                        <td>Man</td>
                                    <?php else : ?>
                                        <td>Woman</td>
                                    <?php endif; ?>

                                    <td><?= $row["kota_asal"] ?></td>
                                    <td><?= $row["kota_lahir"].", ".$row["lahir"] ?></td>
                                    <td><a href="http://localhost/skripsi_coba1/riwayat/<?= $row["nim"] ?>/<?= $row["nim"] ?>.txt"><?= $row["nim"] ?>.txt</a></td>
                                
                                </tr>
                                <?php $i++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- =============== container end =============== -->
        </section>


        <!-- =============== how it works =============== -->
        <section id="how-it-works" class="parallax">
            <!-- =============== container =============== -->
            <div class="container">
                <span class="angle2"></span>
                <span class="angle"></span>
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 wow bounceIn animated headding" data-wow-delay=".5s">
                        <h2>How it <span>works</span></h2>
                        <p>This automatic presence system is built from double verification between Face Recognition and Face Anti-Spoofing.</p>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="row">
                            <div class="col-xs-10 col-sm-10 col-md-10 wow fadeInLeft animated textright" data-wow-delay=".5s">
                                <h3>Add User</h3>
                                <p>Ease of registration which only requires one face photo.</p>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 wow fadeInRight animated" data-wow-delay=".5s">
                                <!-- <i class="fa fa-apple iconfont"></i> -->
                                <i class="fa fa-user-plus iconfont"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-sm-10 col-md-10 wow fadeInLeft animated textright" data-wow-delay=".6s">
                                <h3>Database</h3>
                                <p>Data storage in MySQL database.</p>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 wow fadeInRight animated" data-wow-delay=".6s">
                                <!-- <i class="fa fa-rocket iconfont"></i> -->
                                <i class="fa fa-database iconfont"></i>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-xs-10 col-sm-10 col-md-10 wow fadeInLeft animated textright" data-wow-delay=".7s">
                                <h3>Lorem ipsum</h3>
                                <p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum.</p>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 wow fadeInRight animated" data-wow-delay=".7s">
                                <i class="fa fa-film iconfont"></i>
                            </div>

                        </div> -->
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4 wow bounceIn animated textcenter" data-wow-delay=".4s">
                        <!-- <img src="img/slide-bg.png" alt="slide-bg" /> -->
                        <img src="img/facereg2.png" alt="slide-bg" />
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <!-- <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 wow fadeInLeft animated" data-wow-delay=".5s">
                                <i class="fa fa-android iconfont2"></i>
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-10 wow fadeInRight animated textleft" data-wow-delay=".5s">
                                <h3>Lorem ipsum</h3>
                                <p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum.</p>
                            </div>

                        </div> -->
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 wow fadeInLeft animated" data-wow-delay=".6s">
                                <!-- <i class="fa fa-css3 iconfont2"></i> -->
                                <i class="fa fa-user-secret iconfont2"></i>
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-10 wow fadeInRight animated textleft" data-wow-delay=".6s">
                                <h3>Anti-Spoofing</h3>
                                <p>The anti-spoofing system increases the security of the system from fraud and manipulation.</p>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 wow fadeInLeft animated" data-wow-delay=".7s">
                                <i class="fa fa-users iconfont2"></i>
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-10 wow fadeInRight animated textleft" data-wow-delay=".7s">
                                <h3>Multiple User</h3>
                                <p>Can be used by hundreds of users.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- =============== container end =============== -->
        </section>


        <!-- =============== Screenshots =============== -->
        <section id="Screenshots" class="">
            <!-- =============== container =============== -->
            <div class="container">
                <span class="angle2"></span>
                <span class="angle"></span>
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 wow bounceIn animated headding" data-wow-delay=".1s">
                        <h2>Screen <span>Shots</span></h2>
                        <p>Display your mobile apps awesome features with icon lists and an image carousel of each page. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
                            volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 wow zoomIn animated textright" data-wow-delay=".1s">
                                <div class="span12">

                                    <div id="owl-demo" class="owl-carousel">
                                        <div class="item">
                                            <div class="imghover" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><img src="img/owl1.jpg" alt="Owl Image">
                                                <div class="hover-bg"><i class="fa fa-camera camera"></i></div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="imghover" data-toggle="modal" data-target="#exampleModa2" data-whatever="@mdo">
                                                <img src="img/owl2.jpg" alt="Owl Image">
                                                <div class="hover-bg"><i class="fa fa-camera camera"></i></div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="imghover" data-toggle="modal" data-target="#exampleModa3" data-whatever="@mdo">
                                                <img src="img/owl3.jpg" alt="Owl Image">
                                                <div class="hover-bg"><i class="fa fa-camera camera"></i></div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="imghover" data-toggle="modal" data-target="#exampleModa4" data-whatever="@mdo">
                                                <img src="img/owl4.jpg" alt="Owl Image">
                                                <div class="hover-bg"><i class="fa fa-camera camera"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- =============== popup large image =============== -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog" role="document">
                                    <img src="img/owl1.jpg" alt="Owl Image">
                                </div>
                            </div>

                            <div class="modal fade" id="exampleModa2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabe2">
                                <div class="modal-dialog" role="document">
                                    <img src="img/owl2.jpg" alt="Owl Image">
                                </div>
                            </div>

                            <div class="modal fade" id="exampleModa3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabe3">
                                <div class="modal-dialog" role="document">
                                    <img src="img/owl3.jpg" alt="Owl Image">
                                </div>
                            </div>

                            <div class="modal fade" id="exampleModa4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabe4">
                                <div class="modal-dialog" role="document">
                                    <img src="img/owl4.jpg" alt="Owl Image">
                                </div>
                            </div>
                            <!-- =============== popup large image end =============== -->
                        </div>

                    </div>
                </div>
            </div>
            <!-- =============== container end =============== -->
        </section>
        
        <!-- =============== Registration ===============
        <section id="Reg" class="">
            =============== container ===============
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 wow zoomIn animated headding" data-wow-delay=".1s">
                        <h2>Regist<span>ration</span></h2>
                        <p>Don't have account yet? Please join us by filling in the personal data below</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 wow bounceIn animated" data-wow-delay=".1s">
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="ajax-hidden">

                                <div class="form-group wow fadeInUp animated">
                                    <label for="nim" class="sr-only">SID</label>
                                    <input type="text" placeholder="SID" name="nim" class="form-control" id="nim">
                                </div>

                                <div data-wow-delay=".1s" class="form-group wow fadeInUp animated">
                                    <label for="nama" class="sr-only">Full Name</label>
                                    <input type="text" placeholder="Full Name" name="nama" class="form-control" id="nama">
                                </div>

                                <div data-wow-delay=".2s" class="form-group wow fadeInUp animated">
                                    <label for="fakultas" class="sr-only">Faculty</label>
                                    <input type="text" placeholder="Faculty" name="fakultas" class="form-control" id="fakultas">
                                </div>

                                <div data-wow-delay=".3s" class="form-group wow fadeInUp animated">
                                    <label for="progdi" class="sr-only">Study Program</label>
                                    <input type="text" placeholder="Study Program" name="progdi" class="form-control" id="progdi">
                                </div>

                                <div data-wow-delay=".4s" class="form-group wow fadeInUp animated">
                                    <label for="angkatan" class="sr-only">Class of</label>
                                    <input type="text" placeholder="Class of" name="angkatan" class="form-control" id="angkatan">
                                </div>

                                <div data-wow-delay=".5s" class="form-group wow fadeInUp animated">
                                    <label for="kota_asal" class="sr-only">Hometown</label>
                                    <input type="text" placeholder="Hometown" name="kota_asal" class="form-control" id="kota_asal">
                                </div>

                                <div data-wow-delay=".6s" class="form-group wow fadeInUp animated">
                                    <label for="kota_lahir" class="sr-only">Place of Birth</label>
                                    <input type="text" placeholder="Place of Birth" name="kota_lahir" class="form-control" id="kota_lahir">
                                </div>
                                <div data-wow-delay=".7s" class="form-group wow fadeInUp animated">
                                    <label for="lahir" class="sr-only">Date of Birth</label>
                                    <input type="date" name="lahir" id="lahir" value="" class="form-control" min="1900-01-01">
                                </div>


                                <div data-wow-delay=".8s" class="form-group wow fadeInUp animated">
                                    <label for="sex">Gender : </label>
                                    <input type="radio" name="sex" value="1"><label>&nbsp;Man&nbsp;&nbsp;&nbsp;</label>
                                    <input type="radio" name="sex" value="0"><label>&nbsp;Woman</span></label>
                                </div>
                                <div data-wow-delay=".9s" class="form-group wow fadeInUp animated">
                                    <label for="gambar" class="sr-only">Photo</label>
                                    <input type="file" name="gambar" id="gambar" class="form-control">
                                </div>

                                <button data-wow-delay="1s" class="btn btn-sm btn-block wow fadeInUp animated" type="submit" name="submit">SUBMIT</button>
                            </div>
                            <div class="ajax-response"></div>
                        </form>
                        <br><br>
                    </div>

                    <div class="col-xs-12 col-sm-3 col-md-4 wow bounceIn animated map" data-wow-delay=".5s">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d6508922.473104964!2d-123.76275651635396!3d37.19583981824279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sCalifornia%2C+United+States!5e0!3m2!1sen!2sin!4v1450994260631" width="100%" height="250"
                            frameborder="0" style="border:0" allowfullscreen></iframe>

                    </div>

                    <div class="col-xs-12 col-sm-3 col-md-4 wow bounceIn animated" data-wow-delay=".6s">

                        <section id="text-15" class="widget widget_text">
                            <h3 class="widget-title">California, United States</h3>
                            <div class="textwidget">785, Firs Avenue, place Mall,<br>
                                <p>Tel: 01 234-56786<br> Mobile: 01 234-56786<br> E-mail: <a href="#">info@templatestock.com</a></p>
                                <a href="#">Get directions on the map</a> →</div>
                        </section>

                    </div>
                </div>
            </div>
            =============== container end ===============
        </section> -->
        <!-- =============== Contact =============== -->
        <section id="contact">
            <!-- =============== container =============== -->
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 wow bounceIn animated headding" data-wow-delay=".1s">
                        <h2>Contact <span>Us</span></h2>
                        <p>Display your mobile apps awesome features with icon lists and an image carousel of each page. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
                            volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-4 col-md-4 wow bounceIn animated" data-wow-delay=".1s">

                        <!-- <form>
                            <div class="ajax-hidden">
                                <div class="form-group wow fadeInUp animated">
                                    <label for="c_name" class="sr-only">Name</label>
                                    <input type="text" placeholder="Name" name="c_name" class="form-control" id="c_name">
                                </div>

                                <div data-wow-delay=".1s" class="form-group wow fadeInUp animated">
                                    <label for="c_email" class="sr-only">Email</label>
                                    <input type="email" placeholder="E-mail" name="c_email" class="form-control" id="c_email">
                                </div>

                                <div data-wow-delay=".2s" class="form-group wow fadeInUp animated">
                                    <textarea placeholder="Message" rows="7" name="c_message" id="c_message" class="form-control"></textarea>
                                </div>

                                <button data-wow-delay=".3s" class="btn btn-sm btn-block wow fadeInUp animated" type="submit">Send Message</button>
                            </div>
                            <div class="ajax-response"></div>
                        </form> -->
                        <div class="badge-base LI-profile-badge" data-locale="en_US" data-size="medium" data-theme="dark" data-type="VERTICAL" data-vanity="jane-chrestella-marutotamtama" data-version="v1"><a class="badge-base__link LI-simple-link" href="https://id.linkedin.com/in/jane-chrestella-marutotamtama?trk=profile-badge"> </a></div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-3 col-md-4 wow bounceIn animated map" data-wow-delay=".5s">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1978.6572067904165!2d110.49932091619031!3d-7.31853463828657!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a79c10062ef89%3A0x935ee5fe27504b6d!2sGedung%20C%20UKSW!5e0!3m2!1sid!2sid!4v1596116913618!5m2!1sid!2sid" width="100%" height="100%"
                            frameborder="0" style="border:0" allowfullscreen></iframe>

                        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d6508922.473104964!2d-123.76275651635396!3d37.19583981824279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sCalifornia%2C+United+States!5e0!3m2!1sen!2sin!4v1450994260631" width="100%" height="250"
                            frameborder="0" style="border:0" allowfullscreen></iframe> -->

                        <!-- <script src="https://platform.linkedin.com/badges/js/profile.js" async defer type="text/javascript"></script> -->
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-4 wow bounceIn animated" data-wow-delay=".6s">
                        <section id="text-15" class="widget widget_text">
                            <h3 class="widget-title">FTEK UKSW</h3>
                            <div class="textwidget">Gedung C Jalan Diponegoro 52-60 Salatiga,<br>
                                <p>Tel: 0298-321212 ext 1246<br> E-mail: <a href="mailto:ftek@uksw.edu">ftek@uksw.edu</a></p>
                                <!-- <a href="#">Get directions on the map</a> →</div> -->
                        </section>
                    </div>
                    
                    <!-- <div class="col-xs-12 col-sm-4 col-md-4 wow bounceIn animated map" data-wow-delay=".5s">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d6508922.473104964!2d-123.76275651635396!3d37.19583981824279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sCalifornia%2C+United+States!5e0!3m2!1sen!2sin!4v1450994260631" width="100%" height="250"
                            frameborder="0" style="border:0" allowfullscreen></iframe>

                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 wow bounceIn animated" data-wow-delay=".6s">

                        <section id="text-15" class="widget widget_text">
                            <h3 class="widget-title">California, United States</h3>
                            <div class="textwidget">785, Firs Avenue, place Mall,<br>
                                <p>Tel: 01 234-56786<br> Mobile: 01 234-56786<br> E-mail: <a href="#">info@templatestock.com</a></p>
                                <a href="#">Get directions on the map</a> →</div>
                        </section>

                    </div> -->
                </div>
            </div>
            <!-- =============== container end =============== -->
        </section>
        <!-- Footer -->
        <footer id="footer">
            <!-- =============== container =============== -->
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">

                        <ul class="social-links">
                            <li><a class="wow fadeInUp animated" href="https://www.linkedin.com/in/jane-chrestella-marutotamtama" style="visibility: visible; animation-name: fadeInUp;"><i class="fa fa-linkedin"></i></a></li>
                            <li><a data-wow-delay=".1s" class="wow fadeInUp animated" href="mailto:janecmarutotamtama@gmail.com" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fa fa-envelope"></i></a></li>
                            <li><a data-wow-delay=".2s" class="wow fadeInUp animated" href="https://wa.me/6287831100029" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fa fa-whatsapp"></i></a></li>
                            <li><a data-wow-delay=".4s" class="wow fadeInUp animated" href="https://www.instagram.com/invites/contact/?i=hgaileeysc9w&utm_content=szb4vi" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;"><i class="fa fa-instagram"></i></a></li>
                            <li><a data-wow-delay=".5s" class="wow fadeInUp animated" href="https://www.facebook.com/jane.angelova" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;"><i class="fa fa-facebook"></i></a></li>
                        </ul>

                        <p class="copyright">
                            &copy; 2022 Jane Chrestella Marutotamtama FTEK UKSW
                        </p>
                        <p class="copyright">
                            &copy; 2016 Canvas
                        </p>
                        

                    </div>
                </div>
            </div>
            <!-- =============== container end =============== -->
        </footer>
        <script src="js/script.js"></script>
        <script src="https://platform.linkedin.com/badges/js/profile.js" async defer type="text/javascript"></script>
        <!-- =============== jQuery =============== -->
        <script src="js/jquery.js"></script>
        <!-- =============== Bootstrap Core JavaScript =============== -->
        <script src="js/bootstrap.min.js"></script>
        <!-- =============== Plugin JavaScript =============== -->
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/jquery.fittext.js"></script>
        <script src="js/wow.min.js"></script>
        <!-- =============== Custom Theme JavaScript =============== -->
        <script src="js/creative.js"></script>
        <!-- =============== owl carousel =============== -->
        <script src="owl-carousel/owl.carousel.js"></script>
        <script>
            $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                    autoPlay: 3000,
                    items: 3,
                    itemsDesktop: [1199, 3],
                    itemsDesktopSmall: [979, 3]
                });

            });
        </script>
    </body>

    </html>